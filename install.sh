 #!/bin/bash 

echo 'Installing required software packages'
sudo apt-get update
sudo apt-get install vim tmux

echo 'Creating symbolic links for the config files'
ln -s .bashrc ~/.bashrc
ln -s .tmux.conf ~/.tmux.conf

#fasd
wget https://github.com/clvv/fasd/tarball/1.0.1
tar xvzf 1.0.1
cd clvv-fasd-4822024 
sudo make install
cd ..
rm -rf clvv-fasd-4822024
rm 1.0.1
